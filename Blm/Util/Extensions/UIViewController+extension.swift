//
//  UIViewController+extension.swift
//  Blm
//
//  Created by Jesus on 21/07/21.
//

import Foundation
import AVFoundation
import UIKit

// MARK: Extensión para instanciar ventanas de un storyboard y su UIViewcontroller correspondiente
extension UIViewController {
    class var storyboardID : String {
        let n = "\(self)"
        return n
    }
    
    static func instantiateVC(fromAppStoryboard appStoryboard: Boards) -> Self {
        let x = appStoryboard.viewController(viewControllerClass: self)
        return x
    }
}
extension UIViewController {

    /// Poner el `viewController` sobre el actual y al finalizar lanzar `completion`
    func pushVC(viewController vc:UIViewController, animated: Bool=true, completion: (()->())?=nil) {
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(vc, animated: true)
        completion?()
    }
    /// Quitar por medio de pop el view actual
    func popCurrentView(){
        if navigationController?.popViewController(animated: true) == nil {
        dismiss(animated: true, completion: nil)
    }

    }
    
    func pushLeerCodigoQR(){
        let vc = LeerCodigoQRViewController.instantiateVC(fromAppStoryboard: .LeerCodigoQR)
        self.pushVC(viewController: vc, animated: true, completion: {print("Vista lectura QR")})
    }
    func openURL(url:String) {
        let vc = WebViewController.instantiateVC(fromAppStoryboard: .WebView)
        vc.urlCarga = url
        self.pushVC(viewController: vc, animated: true, completion: {print("Se abre WebView")})
    }
    func pushListadoProductos(){
        let vc = ListaProductosViewController.instantiateVC(fromAppStoryboard: .RecuperarListaProductos)
        self.pushVC(viewController: vc, animated: true, completion: {print("VistaRecuperaListaProductos")})
    }
    func pushDetail(lista:[ListaProductoItem],indexItem:Int){
        let vc = DetailViewController.instantiateVC(fromAppStoryboard: .Detail)
        vc.lista = lista
        vc.indexItem = indexItem
        self.pushVC(viewController: vc, animated: true, completion: {print("Detail")})
    }
    func pushInicio(){
        let vc = HomeViewController.instantiateVC(fromAppStoryboard: .Home)
        self.pushVC(viewController: vc, animated: true, completion: {print("Home")})
    }
    func showMensaje(cuerpo:String, titulo:String){
            let alert = UIAlertController(title: titulo, message: cuerpo, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true)
    }
    func showLoader(){
        let alert = UIAlertController(title: nil, message: "Cargando", preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    /// Permisos
    func hasPermissionsForCamera(completion: (() -> Void)? = nil) -> Bool {
        let s = AVCaptureDevice.authorizationStatus(for: .video)
        switch(s) {
        case .notDetermined, .restricted, .denied:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (videoGranted: Bool) -> Void in
                if videoGranted {
                    // Not access
                } else {
                    self.showMensaje(cuerpo: "Por favor, en la configuración de la aplicación habilite los permisos de cámara", titulo: "Atención")
                }
            })
            return false
        default:
            return true
        }
    }
}
// MARK: toolbar 
extension UIViewController {
    func navBarSettings(titulo:String, isBack:Bool) {
         self.navigationController?.navigationBar.barTintColor        = UIColor.blue
         self.navigationController?.navigationBar.prefersLargeTitles  = false
         self.navigationController?.navigationBar.tintColor           = .white
         self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
         self.navigationItem.title                                    = titulo
        if !isBack{
        self.navigationItem.hidesBackButton = true
        }
     }
}

