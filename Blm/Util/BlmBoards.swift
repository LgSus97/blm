//
//  BlmBoards.swift
//  Blm
//
//  Created by Jesus on 21/07/21.
//

import Foundation

import UIKit


enum Boards: String {
    /// Home storyboard
    case Home
    /// LeerCodigoQR storyboard
    case LeerCodigoQR
    /// QRScanner storyboard
    case QRScanner
    /// WebView storyboard
    case WebView
    /// RecuperarListaProductos  storyboard
    case RecuperarListaProductos
    /// Detail  storyboard
    case Detail

    private var instance : UIStoryboard {
        let id = UIStoryboard(name: self.rawValue, bundle: Bundle.main)
        return id
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type,
                                              function : String = #function, line : Int = #line, file : String = #file) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController con identificador \(storyboardID), Storyboard no encontrado \(self.rawValue).\nArchivo : \(file) \nNúmero de Línea : \(line) \nFunción : \(function)")
        }
        return scene
    }
}
