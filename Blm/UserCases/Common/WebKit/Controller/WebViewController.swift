//
//  WebViewController.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import UIKit
import WebKit

class WebViewController: UIViewController,WKUIDelegate,WKNavigationDelegate {
    var urlCarga:String? = nil
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navBarSettings(titulo: "WebView", isBack: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        processURL()
    }
    func processURL(){
        if let strUrl = urlCarga {
            let url = URL(string: strUrl)!
            let request = URLRequest(url: url)
            webView.load(request)
        } else {
            let url = URL(string:"https://www.google.com")
            let request = URLRequest(url: url!)
            webView.load(request)
        }
    }
}

