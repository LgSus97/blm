//
//  VCQRScanner.swift
//  Blm
//
//  Created by Jesus on 21/07/21.
//

import Foundation
import UIKit


class VCQRScanner:UIViewController{
    var readedCodeIs: ((String)->())?
    @IBOutlet weak var qrScanView: QRScannerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        debugPrint("is running \(qrScanView.isRunning)")
        qrScanView.delegate = self

    }
}


extension VCQRScanner:QRScannerViewDelegate{
    func qrScanningDidFail() {
        debugPrint("fallo el lector de qr")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        debugPrint("code readed: \(str!)")
        popCurrentView()
        self.readedCodeIs!(str!)
    }
    
    func qrScanningDidStop() {
        debugPrint("se detubo el scanning")
    }
}
