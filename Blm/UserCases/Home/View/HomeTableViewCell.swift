//
//  HomeTableViewCell.swift
//  Blm
//
//  Created by Jesus on 21/07/21.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    @IBOutlet weak var rowImgView: UIImageView!
    @IBOutlet weak var rowLabel: UILabel!
    @IBOutlet weak var nextStepImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
