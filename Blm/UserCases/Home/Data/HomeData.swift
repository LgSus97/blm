//
//  HomeData.swift
//  Blm
//
//  Created by Jesus on 21/07/21.
//

import Foundation

class HomeData{
    
    static func getHomeData()->[HomeModel]{
        var arrayData =  [HomeModel]()
        arrayData = [
            HomeModel(textRow: "Leer código QR", imgLeft: "qrCode", imgRight: "next"),
            HomeModel(textRow: "Recuperar lista de productos", imgLeft: "list", imgRight: "next")
        ]
        
        return arrayData
    }
}
