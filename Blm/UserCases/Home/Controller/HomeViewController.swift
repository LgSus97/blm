//
//  HomeViewController.swift
//  Blm
//
//  Created by Jesus on 21/07/21.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var tableViewHome: UITableView!
    
    
    var  arrayData = [HomeModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navBarSettings(titulo: "Home", isBack: false)
    }

    override func viewDidAppear(_ animated: Bool) {
        arrayData = HomeData.getHomeData()
        tableViewHome.reloadData()
    }
    

}
extension HomeViewController: UITableViewDataSource,UITableViewDelegate{
    func configureUI(){
        tableViewHome.delegate = self
        tableViewHome.dataSource = self
        tableViewHome.backgroundColor = UIColor.white
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Aquí retorna el numbero de rows
        return arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HomeTableViewCell
        // Aquí se construye la celda(s)
        cell.rowImgView.image = UIImage(named: arrayData[indexPath.row].imgLeft)
        cell.rowLabel.text = arrayData[indexPath.row].textRow
        cell.nextStepImgView.image = UIImage(named:arrayData[indexPath.row].imgRight )
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            if hasPermissionsForCamera(){
                pushLeerCodigoQR()
            }else{
                showMensaje(cuerpo: "Por favor, en la configuración de la aplicación habilite los permisos de cámara", titulo: "Atención")
            }
        case 1:
            pushListadoProductos()
        default:
            break
        }
    }
    
}
