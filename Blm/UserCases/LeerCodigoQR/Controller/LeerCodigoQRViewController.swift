//
//  LeerCodigoQRViewController.swift
//  Blm
//
//  Created by Jesus on 21/07/21.
//

import UIKit

class LeerCodigoQRViewController: UIViewController {

    @IBOutlet weak var TxtLectura: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navBarSettings(titulo: "QR Scanner", isBack: true)
    }


    func validateURL(resultQR:String){
        if resultQR.isValidURL {
            self.dismiss(animated: true, completion: { [self] in
                openURL(url:resultQR)
            })
        }else{
            TxtLectura.text = resultQR
        }
    }

    

    @IBAction func leerCodigoAction(_ sender: Any) {
        let v = VCQRScanner.instantiateVC(fromAppStoryboard: .QRScanner)
        v.readedCodeIs = { [self] code in
            debugPrint(code)
            validateURL(resultQR: code)
        }
        self.pushVC(viewController: v)

    }
    

}
