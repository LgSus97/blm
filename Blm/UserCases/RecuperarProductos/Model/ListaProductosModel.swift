//
//  ListaProductosModel.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import Foundation

//MARK: models to parse data from response

 struct ListaProductosModel {

    private var meta:MetaModel?
    private var data:[DatumModel] = []
    
    init() {}
    
    init(dictionary: JSONDictionaryBLM){
        let respuestaDictionary = dictionary["meta"] as? JSONDictionaryBLM
        if let m = respuestaDictionary {
            meta = MetaModel(dictionary: m)
        }
        if let s = dictionary["data"] as? [JSONDictionaryBLM] {
            for item in s {
                data.append(DatumModel.init(dictionary: item as JSONDictionaryBLM))
            }
        }
    }
    
    init(viewModel: ListaResponse) {
        meta = viewModel.getMeta()
        data = viewModel.getData()!
    }
    
    //MARK: set methods
    
    public mutating func setMeta(meta:MetaModel){
        self.meta = meta
    }
    public mutating func setData(data:[DatumModel]){
        self.data = data
    }
    //MARK: get methods
    public func getMeta()->MetaModel?{
        return self.meta
    }
    public func getData()->[DatumModel]?{
        return data
    }

    
}


 struct MetaModel {
    private var pagination: PaginationModel?
    
    init() {}
    init(dictionary: JSONDictionaryBLM) {
        let respuestaDictionary = dictionary["pagination"] as? JSONDictionaryBLM
        if let p = respuestaDictionary {
            pagination = PaginationModel(dictionary: p)
        }
    }
    //MARK: set methods
    public mutating func setPagination(pagination:PaginationModel){
        self.pagination = pagination
    }
    //MARK: get methods
    public func getPagination()->PaginationModel?{
        return self.pagination
    }

}
struct PaginationModel {

    
    private var total:Int?
    private var pages:Int?
    private var page:Int?
    private var limit:Int?
    private var links: LinksModel?
    
    init() {}
    init(dictionary: JSONDictionaryBLM) {
        total = dictionary["total"] as? Int
        pages = dictionary["pages"] as? Int
        page  = dictionary["page"] as? Int
        limit = dictionary["limit"] as? Int
        let respuestaDictionary = dictionary["links"] as? JSONDictionaryBLM
        if let l = respuestaDictionary {
            links = LinksModel(dictionary: l)
        }
    }
    //MARK: set methods
    public mutating func setTotal(total:Int){
        self.total = total
    }
    public mutating func setPages(pages:Int){
        self.pages = pages
    }
    public mutating func setPage(page:Int){
        self.page = page
    }
    public mutating func setLimit(limit:Int){
        self.limit = limit
    }
    public mutating func setLimit(links:LinksModel){
        self.links = links
    }
    //MARK: get methods
    public func getTotal()->Int?{
        return self.total
    }
    public func getPages()->Int?{
        return self.pages
    }
    public func getPage()->Int?{
        return self.page
    }
    public func getLimit()->Int?{
        return self.limit
    }
    public func getLinks()->LinksModel?{
        return self.links
    }

}
struct DatumModel {
    private var id: Int?
    private var name: String?
    private var email: String?
    private var gender: String?
    private var status: String?
    
    init() {}
    init(dictionary: JSONDictionaryBLM) {
        id     = dictionary["id"] as? Int
        name   = dictionary["name"] as? String
        email  = dictionary["email"] as? String
        gender = dictionary["gender"] as? String
        status = dictionary["status"] as? String
    }
    //MARK: set methods
    public mutating func setId(id:Int){
        self.id = id
    }
    public mutating func setName(name:String){
        self.name = name
    }
    public mutating func setEmail(email:String){
        self.email = email
    }
    public mutating func setGender(gender:String){
        self.gender = gender
    }
    public mutating func setStatus(status:String){
        self.status = status
    }
    
    //MARK: get methods
    public func getId()->Int?{
        return self.id
    }
    public func getName()->String?{
        return self.name
    }
    public func getEmail()->String?{
        return self.email
    }
    public func getGender()->String?{
        return self.gender
    }
    public func getStatus()->String?{
        return self.status
    }
}
struct LinksModel{
    private var previous: String?
    private var current:  String?
    private var next: String?
    
    init() {}
    init(dictionary: JSONDictionaryBLM) {
        previous = dictionary["previous"] as? String
        current  = dictionary["current"] as? String
        next     = dictionary["next"] as? String
    }
    //MARK: set methods
    public mutating func setPrevious(previous:String){
        self.previous = previous
    }
    public mutating func setCurrent(current:String){
        self.current = current
    }
    public mutating func setNext(next:String){
        self.next = next
    }
    //MARK: get methods
    public func getPrevious()->String?{
        return self.previous
    }
    public func getCurrent()->String?{
        return self.current
    }
    public func getNext()->String?{
        return self.next
    }
}
