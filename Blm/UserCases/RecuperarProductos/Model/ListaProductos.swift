//
//  ListaProductos.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import Foundation


public class ListaProductos:NSObject {
    @objc dynamic private(set) var response:ListaResponse = ListaResponse()
    @objc dynamic private(set) var sourceError:ErrorResponse!
    private var token1:NSKeyValueObservation?
    private var request:ListaProductosRequest
    public var postListaProductos :(() -> ()) = {  }
    
    public init(request:ListaProductosRequest){
        self.request = request
        super.init()
        
        populateSources()
    }
        
    func invalidateObservers() {
        self.token1?.invalidate()
    }
    
    func populateSources() {
        
        self.request.loadSources { [unowned self] sources in
            switch sources{
            case .success(let resultJson):
                self.token1 = self.observe(\.response) { _,_ in
                    self.postListaProductos()
                }
                let json = resultJson as! ListaProductosModel
                self.response = ListaResponse.init(source: json)
                break
            case .failure(let failure) :
                self.token1 = self.observe(\.sourceError) { _,_ in
                    self.postListaProductos()
                }
                self.sourceError = errorReport.shareErrorReport.report(value: failure)
            }
        }
    }
}

public class ListaResponse:NSObject{
    
    private var meta:MetaModel?
    private var data:[DatumModel]?

    override init() {}
    
    init(source: ListaProductosModel){
        meta = source.getMeta()
        data = source.getData()
    }

    func setMeta(meta:MetaModel){
        self.meta = meta
    }
    func setData(data:[DatumModel]){
        self.data = data
    }
    func getMeta()->MetaModel?{
        return self.meta
    }
    func getData()->[DatumModel]?{
        return data
    }
}
