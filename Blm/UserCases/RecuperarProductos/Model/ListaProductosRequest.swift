//
//  ListaProductosRequest.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import Foundation

public class ListaProductosRequest:RequestManager {

    func loadSources(completion :@escaping (ApiResult) -> ()) {
        let urlsBlm = UrlsBlm.shareManager
        var request:NSMutableURLRequest = NSMutableURLRequest()
        request = blmRequestManagerGet(url: urlsBlm.getURL())
        let session = URLSession.shared // URLSession es asincriono
        
        ResponseService.sharedResponse.response(url: urlsBlm.getURL(),
                                                request: request,
                                                session: session,
                                                completion: completion)
        
    }
}
