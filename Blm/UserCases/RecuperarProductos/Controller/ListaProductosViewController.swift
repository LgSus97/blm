//
//  ListaProductosViewController.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import UIKit

private var listaProductos:[ListaProductoItem] = []

class ListaProductosViewController: UIViewController {

    @IBOutlet weak var tableListaProductos: UITableView!
    
    var webserviceListaProductos :ListaProductosRequest?
    private var sourceListViewModelListaProductos : ListaProductos!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navBarSettings(titulo: "Lista de productos", isBack: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        callAPI()
    }
    func callAPI(){
        showLoader()
        self.webserviceListaProductos = ListaProductosRequest()
        self.sourceListViewModelListaProductos = ListaProductos(request: self.webserviceListaProductos!)
        self.sourceListViewModelListaProductos.postListaProductos = {
            if self.sourceListViewModelListaProductos!.sourceError != nil{
                self.dismiss(animated: true, completion:{ [self] in
                    showMensaje(cuerpo: "Atención", titulo: "Por el momento el servicio no se encuentra disponible")
                })
            }else{
                self.dismiss(animated: true, completion: { [self] in
                    if self.sourceListViewModelListaProductos.response.getData()!.count > 0 {
                        let resp = self.sourceListViewModelListaProductos.response
                        listaProductos = []
                        for item in resp.getData()!{
                            let p = ListaProductoItem(
                                id: item.getId(),
                                name: item.getName(),
                                email: item.getEmail(),
                                gender: item.getGender(),
                                status: item.getStatus())
                            listaProductos.append(p)
                        }
                        tableListaProductos.reloadData()
                    }else{
                        showMensaje(cuerpo: "Atención", titulo: "Por el momento el servicio no se encuentra disponible")
                    }
                })
            }
        }
    }


}
extension ListaProductosViewController:UITableViewDelegate, UITableViewDataSource{
    func configureUI()  {
        tableListaProductos.delegate = self
        tableListaProductos.dataSource = self
        tableListaProductos.register(UINib(nibName: "CellRow", bundle: nil), forCellReuseIdentifier: "CellRow")
        tableListaProductos.backgroundColor = UIColor.white
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaProductos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = listaProductos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellRow", for: indexPath) as! CellRowTVC
        
        cell.TxtNombre.text = item.name ?? ""
        cell.TxtId.text = String(item.id ?? 0)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showLoader()
        self.dismiss(animated: true, completion: { [self] in
            pushDetail(lista: listaProductos,
                       indexItem: indexPath.row)
        })
    }
    
}
