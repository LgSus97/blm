//
//  DetailViewController.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import UIKit
private var listaDetalle:[ListaProductoItem] = []
class DetailViewController: UIViewController {
    

    @IBOutlet weak var TxtNombre: UILabel!
    @IBOutlet weak var TxtEmail: UILabel!
    @IBOutlet weak var TxtGender: UILabel!
    @IBOutlet weak var TxtStatus: UILabel!
    @IBOutlet weak var TxtId: UILabel!
    
    var lista:[ListaProductoItem] = []
    var indexItem  = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarSettings(titulo: "Detalle", isBack: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        listaDetalle = lista
    }
    override func viewDidAppear(_ animated: Bool) {
        updateData()
    }
    
    func updateData(){
        self.dismiss(animated: true, completion: { [self] in
            let detail = listaDetalle[indexItem]
            TxtNombre.text  = detail.name ?? ""
            TxtEmail.text   = detail.email ?? ""
            TxtGender.text  = detail.gender ?? ""
            TxtStatus.text  = detail.status ?? ""
            TxtId.text      = String(detail.id ?? 0)
        })
    }
    
    @IBAction func volverInicioAction(_ sender: Any) {
        pushInicio()
    }
    
}
