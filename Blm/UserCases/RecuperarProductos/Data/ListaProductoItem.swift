//
//  ListaProductoItem.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import Foundation

struct ListaProductoItem{
    var id:Int?,
        name:String?,
        email:String?,
        gender:String?,
        status:String?
}
