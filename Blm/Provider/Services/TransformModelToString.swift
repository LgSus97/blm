//
//  TransformModelToString.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import Foundation

class TransformModelToString {
    func stringTojson(value:String) -> [String:Any] {
        let data = value.data(using: .utf8)!
        var jsonArray:[String:Any] = [:]
        do {
            jsonArray = try (JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any])!

            return jsonArray
        } catch _ as NSError {
            print("Error")
        }
        return jsonArray
    }
}
