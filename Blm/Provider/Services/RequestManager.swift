//
//  RequestManager.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import Foundation

public class RequestManager{
    public func blmRequestManagerGet(url:String)->NSMutableURLRequest{
        let httpMethod:String = "GET"
        
        var request:NSMutableURLRequest = NSMutableURLRequest()
        let url1 = URL(string: url)!

        request = NSMutableURLRequest(url: url1)
        request.httpMethod = httpMethod
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        return request
    }
}
