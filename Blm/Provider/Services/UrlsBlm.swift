//
//  UrlsBlm.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import Foundation

public class UrlsBlm{
    public static let shareManager = UrlsBlm()
    
    private let base     = "https://gorest.co.in/"
    private let context  = "public/v1/"
    private let endPoint = "users"

    public func getURL()->String{
        return base + context + endPoint
    }

}
