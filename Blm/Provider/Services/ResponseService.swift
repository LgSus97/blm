//
//  ResponseService.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import Foundation
typealias JSONDictionaryBLM = [String:Any]


class ResponseService:TransformModelToString {
    static let sharedResponse = ResponseService()

    func response(url:String,request:NSMutableURLRequest,session:URLSession,completion :@escaping (ApiResult) -> ()){
        
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            if (error != nil) {
                #if DEBUG
                print("---------- ERROR for: \(url)")
                print("error=\(String(describing: error))")
                #endif
                DispatchQueue.main.async {
                    completion(ApiResult.failure(.connectionError))
                }
            } else {
                var headlines:Any!
                
                let httpResponse = response as? HTTPURLResponse
                let codigoRespuesta=httpResponse?.statusCode
                
                #if DEBUG
                print("---------- response for: \(url)")
                print("codigoRespuesta=\(String(describing: codigoRespuesta))")
                #endif
                switch Int(codigoRespuesta!){
                case 200:
                    if let data = data {
                       
                        let valor =  String(data: data, encoding: String.Encoding.ascii)  as NSString?
                        let dictionariesObject = self.stringTojson(value: valor! as String)
                   
                        DispatchQueue.main.async {
                            let parts = url.split(separator: "/")
                            let last = parts.last!
                            #if DEBUG
                            debugPrint("last is \(last) with: \(dictionariesObject)")
                            #endif
                            switch last {
                                case "users":
                                    headlines = ListaProductosModel.init(dictionary: dictionariesObject)
                                break
                
                            default:
                                break
                            }
                            DispatchQueue.main.async {
                                completion(ApiResult.success(headlines!))
                            }
                        }
                    }
                    break

                case 404:
                    DispatchQueue.main.async {
                        completion(ApiResult.failure(.notFound))
                    }
                    break
                case 403:
                    DispatchQueue.main.async {
                        completion(ApiResult.failure(.accessDenied))
                    }
                    break
                case 500 ... 599:
                    DispatchQueue.main.async {
                        completion(ApiResult.failure(.serverError))
                    }
                    
                default:
                    DispatchQueue.main.async {
                        completion(ApiResult.failure(.unknownError))
                    }
                }
            }
        }
        dataTask.resume()
    }

 

}
