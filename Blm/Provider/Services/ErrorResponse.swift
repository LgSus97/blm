//
//  ErrorResponse.swift
//  Blm
//
//  Created by Jesus on 22/07/21.
//

import Foundation
public class ErrorResponse:NSObject{
    private var typeError:String?
    private var codigo:Int?
    
    public  init(typeError:String) {
        self.typeError = typeError
    }
    public func getTypeError()->String?{
        return typeError
    }
}
enum ApiResult {
    case success(Any)
    case failure(RequestError)
}
enum RequestError: Error {
    case unknownError
    case connectionError
    case authorizationError(Any)
    case accessDenied
    case invalidRequest
    case notFound
    case invalidResponse
    case serverError
    case serverUnavailable
}
class errorReport{
    static let errorGeneral = "Por el momento el servicio no esta disponible"
    static let shareErrorReport = errorReport()
    @objc dynamic private(set) var sourceError:ErrorResponse!
    func report(value:RequestError)->ErrorResponse {
        switch value {
        case .connectionError:
            self.sourceError = ErrorResponse(typeError: errorReport.errorGeneral)
            break
        case .authorizationError: // 401
            self.sourceError = ErrorResponse(typeError: errorReport.errorGeneral)
            break
        case .accessDenied: // 403
            self.sourceError = ErrorResponse(typeError: errorReport.errorGeneral)
        case .notFound: // 404
            self.sourceError = ErrorResponse(typeError: errorReport.errorGeneral)
            break
        case .serverError: // 500 ... 599
            self.sourceError = ErrorResponse(typeError: errorReport.errorGeneral)
            break
        default: // unknownError
            self.sourceError = ErrorResponse(typeError: errorReport.errorGeneral)
        }
        return sourceError
    }
    
}
